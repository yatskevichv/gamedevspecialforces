#include <iostream>

#include "MySharedLib.h"
#include "MyStaticLib.h"

int main(int, char **, char **env)
{
    std::cout << "Hello World!" << std::endl;

    std::cout << MySharedLib::printAboutMe() << std::endl;

    std::cout << MyStaticLib::printAboutMe() << std::endl;

    return std::cout.fail() ? 1 : 0;
}
